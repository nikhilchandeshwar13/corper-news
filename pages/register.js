import React, { useState } from "react";
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import style from "../styles/Login.module.css";
import axios from "axios";
import { API_URL } from "../constants/URL";
import Header from "./common/Header";
import Footer from "./common/Footer";
import Link from "next/link";
export default function Register() {
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const handleUsername = (e) => {
    setUsername(e.target.value);
  };
  const handleEmail = (e) => {
    setEmail(e.target.value);
  };
  const handlePassword = (e) => {
    setPassword(e.target.value);
  };
  const doSignup = () => {
    if (username === "") {
      alert("Please enter username");
    } else if (email === "") {
      alert("Please enter email address");
    } else if (password === "") {
      alert("Please enter password");
    } else {
      let formdata = new FormData();
      formdata.append("action", "DO_SIGNUP");
      formdata.append("email", email);
      formdata.append("password", password);
      formdata.append("username", username);
      axios({
        method: "post",
        url: `${API_URL}`,
        data: formdata,
      })
        .then(function (response) {
          if (response.data.status === "success") {
            localStorage.setItem("username", username);

            window.location.href = "/";
          } else {
            alert(response.data.message);
          }
        })
        .catch(function (error) {
          console.log("error=====>", error);
        });
    }
  };
  return (
    <div>
      <Container className={style.login_container}>
        <h1 className={style.login_heading}>Sign up</h1>
        <div className={style.login_bottom_option}>
          <span>
            Already have an account?{" "}
            <Link href="/login">
              <a>Login</a>
            </Link>
          </span>
        </div>
        <Row>
          <Col md={3}></Col>
          <Col md={6} className={style.login_form_holder}>
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label className={style.form_label}>Username</Form.Label>
              <Form.Control
                type="email"
                placeholder="username"
                onChange={handleUsername}
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label className={style.form_label}>
                Email address
              </Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                onChange={handleEmail}
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Label lassName={style.form_label}>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                onChange={handlePassword}
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicCheckbox">
              <Form.Check
                type="checkbox"
                label=" Keep me logged in Log In"
                checked
              />
            </Form.Group>
            <button
              variant="primary"
              type="submit"
              className={style.login_button}
              onClick={doSignup}
            >
              Submit
            </button>
            <div className={style.login_bottom_option}>
              <span>
                By creating an account, I accept the{" "}
                <Link href="/terms">
                  <a>terms and conditions</a>
                </Link>
              </span>
            </div>
          </Col>
          <Col md={3}></Col>
        </Row>
      </Container>
    </div>
  );
}
