import React, { useState, useEffect } from "react";
import style from "../styles/Contact.module.css";
import { Container, Row, Col, Image, Form } from "react-bootstrap";
//import Hori from "../assets/hori.png";

import axios from "axios";
import { API_URL } from "../constants/URL";
// import VerticalAds from "../components/VerticalAds";
// import { Link, Redirect } from "react-router-dom";
import {
  FaEnvelope,
  FaTwitter,
  FaInstagram,
  FaLinkedin,
  FaYoutube,
  FaFacebookF,
} from "react-icons/fa";
import Trending from "./common/Trending";
import Header from "./common/Header";
import Footer from "./common/Footer";
// import HorizontalAds from "../components/HorizontalAds";
// import GoogleAd from "../components/GoogleAd";
import Link from "next/link";
export default function ContactUs() {
  const [topThird, setTopThird] = useState([]);
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [message, setMessage] = useState("");
  useEffect(() => {
    getTopThree();
  }, []);
  const getTopThree = () => {
    let formdata = new FormData();
    formdata.append("action", "GET_TRENDING");
    axios({
      method: "post",
      url: `${API_URL}`,
      data: formdata,
    })
      .then(function (response) {
        //

        setTopThird(response.data);
      })
      .catch(function (error) {
        console.log("error=====>", error);
      });
  };
  const handleName = (e) => {
    setName(e.target.value);
  };
  const handleEmail = (e) => {
    setEmail(e.target.value);
  };
  const handlePhone = (e) => {
    setPhone(e.target.value);
  };
  const handleMessage = (e) => {
    setMessage(e.target.value);
  };
  const submitData = () => {
    if (email === "") {
      alert("Please enter email address");
    } else {
      let formdata = new FormData();
      formdata.append("action", "SEND_CONTACT_US");
      formdata.append("email", email);
      formdata.append("name", name);
      formdata.append("phone", phone);
      formdata.append("message", message);
      axios({
        method: "post",
        url: `${API_URL}`,
        data: formdata,
      })
        .then(function (response) {
          if (response.data.status === "success") {
            alert(
              "Your request submitted successfully, we will contact you shortly"
            );
            setName("");
            setEmail("");
            setPhone("");
            setMessage("");
          } else {
            alert(response.data.message);
          }
        })
        .catch(function (error) {
          console.log("error=====>", error);
        });
    }
  };
  return (
    <div>
      <Container>
        <div className={style.main_heading_about}>
          <h3 className={style.about_heading}>Contact Us</h3>
        </div>
        <Container>
          {/* <HorizontalAds type="Contact us Top banner 970 x 90" category="" /> */}
        </Container>
        {/* <GoogleAd slot="5986890511" googleAdId="ca-pub-4422070729845058" /> */}
        <Row>
          <Col md={8}>
            <Container>
              <Row>
                <Col md={8} className={style.inner_contact_us}>
                  <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label className={style.form_label}>Name</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Enter Name"
                      onChange={handleName}
                      value={name}
                    />
                  </Form.Group>
                  <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label className={style.form_label}>
                      Email address*
                    </Form.Label>
                    <Form.Control
                      type="email"
                      placeholder="Enter email"
                      onChange={handleEmail}
                      value={email}
                    />
                  </Form.Group>
                  <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label className={style.form_label}>Phone</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Phone Number"
                      onChange={handlePhone}
                      value={phone}
                    />
                  </Form.Group>

                  <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Message</Form.Label>
                    <Form.Control
                      as="textarea"
                      rows={3}
                      onChange={handleMessage}
                      value={message}
                    />
                  </Form.Group>
                  <button
                    className={style.send_button}
                    onClick={() => submitData()}
                  >
                    Send
                  </button>
                </Col>
                <Col md={8}>
                  <div className={style.contact_form_bottom}>
                    <p className={style.share_message}>
                      Send your personal experience you want to share as news
                      with the world to this WhatsApp number +2348068058455
                      <br></br>
                      Include photos and videos, if any
                    </p>

                    <span className={style.contact_email}>
                      <FaEnvelope
                        color="#f00606"
                        className={style.email_icon}
                      />
                      {""}{" "}
                      <p className={style.contact_email}>info@corpernews.com</p>
                    </span>

                    <ul
                      className={style.social_link}
                      style={{ textAlign: "left" }}
                    >
                      <li className={style.contact_menu_li}>Follow us</li>
                      <li className={style.contact_menu_li}>
                        <a
                          className={style.contact_menu_a}
                          href="https://m.facebook.com/corpernews"
                        >
                          <FaFacebookF />
                        </a>
                      </li>
                      <li className={style.contact_menu_li}>
                        <a
                          className={style.contact_menu_a}
                          href="https://twitter.com/corpernews"
                        >
                          <FaTwitter />
                        </a>
                      </li>
                      <li className={style.contact_menu_li}>
                        <a
                          className={style.contact_menu_a}
                          href="https://www.instagram.com/corpernews/"
                        >
                          <FaInstagram />
                        </a>
                      </li>
                      <li className={style.contact_menu_li}>
                        <a
                          className={style.contact_menu_a}
                          href="https://www.linkedin.com/in/corpernews"
                        >
                          <FaLinkedin />
                        </a>
                      </li>
                      <li className={style.contact_menu_li}>
                        <a
                          className={style.contact_menu_a}
                          href="https://www.youtube.com/channel/UCsgphM-o-WbgJgFHePpsALQ"
                        >
                          <FaYoutube />
                        </a>
                      </li>
                    </ul>
                  </div>
                </Col>
              </Row>
            </Container>
          </Col>
          <Col md={4} className="single-col post-layout3">
            <Row>
              <Col md={12}>
                {/* <VerticalAds /> */}
                <div>{/* <h3 className="trending_text">Trending</h3> */}</div>
                <Row>
                  <Trending />
                </Row>
                {/* <GoogleAd
                  slot="8832545708"
                  googleAdId="ca-pub-4422070729845058"
                /> */}
              </Col>
            </Row>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
