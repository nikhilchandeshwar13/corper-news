import React, { useState, useEffect } from "react";
import { Container, Row, Col } from "react-bootstrap";
//import "../css/NewsDetails.css";

import { FaFolderOpen, FaRegUser, FaRegClock } from "react-icons/fa";
import axios from "axios";
import { API_URL } from "../constants/URL";
import VerticalAds from "./common/VerticalAds";
import Header from "./common/Header";
import Footer from "./common/Footer";
import style from "../styles/Details.module.css";
import Trending from "./common/Trending";
import { Helmet } from "react-helmet";
import {
  FaFacebookF,
  FaTwitter,
  FaInstagram,
  FaLinkedin,
  FaYoutube,
  FaWhatsapp,
  FaUserShield,
  FaAngleLeft,
  FaAngleRight,
} from "react-icons/fa";
import {
  FacebookShareButton,
  FacebookIcon,
  PinterestShareButton,
  PinterestIcon,
  RedditShareButton,
  RedditIcon,
  WhatsappShareButton,
  WhatsappIcon,
  LinkedinShareButton,
  LinkedinIcon,
  TwitterShareButton,
  TwitterIcon,
} from "next-share";
import { useRouter } from "next/router";
import Link from "next/link";
// import RepliedComment from "./common/RepliedComment";
import HorizonralAds from "./common/HorizontalAds";
import MetaTags from "react-meta-tags";
// import DocumentMeta from "react-document-meta";
// import Helmet from "react-helmet";
// import GoogleAd from "../components/GoogleAd";
import Head from "next/head";
export default function Details({ props }) {
  const router = useRouter();
  const id = router.query.id;

  //alert(id);
  //const location = useLocation();
  const [url, setUrl] = useState("");
  const [name, setName] = useState("");
  const [myid, setMyid] = useState("");
  const [category, setCategory] = useState("");
  const [created, setCreated] = useState("");
  const [image, setImage] = useState("");
  const [content, setContent] = useState("");
  const [sid, setSid] = useState("");
  const [next, setNext] = useState("");
  const [prev, setPrev] = useState("");
  const [nextNum, setNextNum] = useState();
  const [prevNum, setPrevNum] = useState();
  const [topThird, setTopThird] = useState([]);
  const [source, setSource] = useState("");
  const [comment, setComment] = useState("");
  const [postComment, setPostComment] = useState([]);
  const [reply, setReply] = useState("");
  const [postTime, setPostTime] = useState("");
  const [embaded, setEmbaded] = useState("");
  const [intro, setIntro] = useState("");
  const [url_one, setUrl_one] = useState("");
  const [newsLoading, setNewsLoading] = useState(true);
  useEffect(() => {
    // alert(id);
    const myString = window.location.pathname.split("/")[2];
    // document.title = name;
    // if (id.includes("?")) {
    //   alert("Yes");
    // } else {
    //   alert("No");
    // }

    const url = window.location.pathname.replace(/ /g, "%20");
    setUrl_one(url);
    // setMyid(id);
    getNewsDetails(id);
    setSid(localStorage.getItem("username"));
    console.log("myid", sid);
    getNext(id);
    getPrev(id);
    setNextNum(parseInt(id) + 1);
    setPrevNum(parseInt(id) - 1);
    getTopThree();
    getComment(id);
    setUrl(url_one);

    //alert(url);
    //alert(sid);
  }, []);

  const handleComment = (e) => {
    setComment(e.target.value);
  };
  const handleRepliedMessgae = (e) => {
    setReply(e.target.value);
  };
  const replyComment = (id) => {
    if (localStorage.getItem("username") === null) {
      alert("Please login to post comment");
    } else {
      if (reply === "") {
        alert("Please write your comment");
      } else {
        let formdata = new FormData();
        formdata.append("action", "POST_REPLY_COMMENT");
        formdata.append("username", localStorage.getItem("username"));
        formdata.append("id", id);
        formdata.append("pid", myid);
        formdata.append("comment", reply);
        axios({
          method: "post",
          url: `${API_URL}`,
          data: formdata,
        })
          .then(function (response) {
            //

            alert(response.data.message);
          })
          .catch(function (error) {
            console.log("error=====>", error);
          });
      }
    }
  };
  const submitComment = () => {
    if (localStorage.getItem("username") === null) {
      alert("Please login to post comment");
    } else {
      if (comment === "") {
        alert("Please write your comment");
      } else {
        let formdata = new FormData();
        formdata.append("action", "POST_COMMENT");
        formdata.append("username", localStorage.getItem("username"));
        formdata.append("pid", myid);
        formdata.append("comment", comment);
        axios({
          method: "post",
          url: `${API_URL}`,
          data: formdata,
        })
          .then(function (response) {
            //

            alert(response.data.message);
          })
          .catch(function (error) {
            console.log("error=====>", error);
          });
      }
    }
  };
  const getComment = (id) => {
    let formdata = new FormData();
    formdata.append("action", "GET_POST_COMMENT");
    formdata.append("id", id);
    axios({
      method: "post",
      url: `${API_URL}`,
      data: formdata,
    })
      .then(function (response) {
        //
        setPostComment(response.data);
      })
      .catch(function (error) {
        console.log("error=====>", error);
      });
  };
  const getTopThree = () => {
    let formdata = new FormData();
    formdata.append("action", "GET_TRENDING");
    axios({
      method: "post",
      url: `${API_URL}`,
      data: formdata,
    })
      .then(function (response) {
        //

        setTopThird(response.data);
      })
      .catch(function (error) {
        console.log("error=====>", error);
      });
  };
  const getNext = (id) => {
    let formdata = new FormData();
    formdata.append("action", "GET_NEXT_LINK");
    formdata.append("id", id);
    axios({
      method: "post",
      url: `${API_URL}`,
      data: formdata,
    })
      .then(function (response) {
        setNext(response.data.next);
      })
      .catch(function (error) {
        console.log("error=====>", error);
      });
  };
  const getPrev = (id) => {
    let formdata = new FormData();
    formdata.append("action", "GET_PREV_LINK");
    formdata.append("id", id);
    axios({
      method: "post",
      url: `${API_URL}`,
      data: formdata,
    })
      .then(function (response) {
        setPrev(response.data.next);
      })
      .catch(function (error) {
        console.log("error=====>", error);
      });
  };
  const getNewsDetails = (id) => {
    let formdata = new FormData();
    formdata.append("action", "GET_NEWS_DETAILS");
    formdata.append("id", id);
    axios({
      method: "post",
      url: `${API_URL}`,
      data: formdata,
    })
      .then(function (response) {
        //
        // window.scrollTo(0, 0);
        setName(response.data.name);
        setCategory(response.data.category);
        setCreated(response.data.created_by_name);
        setImage(response.data.image);
        setContent(response.data.content);
        setSource(response.data.source);
        setPostTime(response.data.time);
        setEmbaded(response.data.embaded);
        setIntro(response.data.intro);
        setNewsLoading(false);
      })
      .catch(function (error) {
        console.log("error=====>", error);
      });
  };
  const openWhatsapp = () => {
    window.location =
      "https://wa.me/?text=https://corpernews24.com" + window.location.pathname;
  };

  return (
    <div className="wrapper">
      <Head>
        <meta charSet="utf-8" />
        {/* <title>{name}</title>
        <meta
          name="description"
          content="Omo-Agege Declares for Governor, Promises to Transform Delta"
        />
        <meta property="og:title" content="CorperNews24" />
        <meta property="og:image" content={image} />
        <meta property="og:image" content={image} />
        <meta property="og:image:secure_url" content={image} />
        <meta property="og:image:type" content="image/jpeg" />
        <meta property="og:image:width" content="400" />
        <meta property="og:image:height" content="300" /> */}

        <title>{props.name}</title>
        <meta name="description" content={props.name} />
        <meta property="og:title" content="CorperNews24" />
        <meta property="og:image" content={props.image} />
        <meta property="og:image" content={props.image} />
        <meta property="og:image:secure_url" content={props.image} />
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:site" content="@CorperNews24" />
        <meta name="twitter:title" content={props.name} />
        <meta name="twitter:description" content={props.intro} />
        <meta name="twitter:image" content={props.image}></meta>
        <meta property="og:image:width" content="400" />
        <meta property="og:image:height" content="300" />
      </Head>

      <div className={style.content}>
        <div>
          <Container
            fluid
            className={style.news_details_horizontal}
            style={{ marginTop: "-20px" }}
          >
            <HorizonralAds type="News Details 970 x 90" category="" />
          </Container>
          {/* <GoogleAd slot="5986890511" googleAdId="ca-pub-4422070729845058" /> */}
          <Container fluid>
            <Row>
              <Col
                md={7}
                className={style.main_content}
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <div className={style.blog_details_content}>
                  <h1 className={style.post_title}>{name}</h1>
                  <ul className={style.post_meta}>
                    <li className={style.li}>
                      <FaFolderOpen className={style.icon} /> Category:{" "}
                      <a
                        // to={{
                        //   pathname: "/category/" + category,
                        // }}
                        // href=""
                        className={style.li_a}
                      >
                        {" "}
                        {category}
                      </a>
                    </li>

                    <li className={style.li}>
                      <FaRegUser className={style.icon} /> By {created}
                    </li>

                    <li className={style.li}>
                      <FaRegClock className={style.icon} />{" "}
                      <a href="" className={style.li_a}>
                        {postTime}
                      </a>
                    </li>
                  </ul>
                  <div className={style.author_box_n_social}>
                    <ul className={style.social_share}>
                      <li className={style.facebook}>
                        <p>Share News</p>
                      </li>
                      <li></li>
                      <li className="facebook">
                        {/* <a
                          href={
                            "https://www.facebook.com/sharer/sharer.php?u=https://corper-news-one-more.vercel.app" +
                            "/details?intro=" +
                            props.intro?.replace(/ /g, "-") +
                            "&id=" +
                            id
                          }
                        >
                          <FaFacebookF />
                        </a> */}

                        <FacebookShareButton
                          url={
                            "https://corper-news-one-more.vercel.app" +
                            "/details?intro=" +
                            props.intro?.replace(/ /g, "-") +
                            "&id=" +
                            id
                          }
                        >
                          <FacebookIcon size={32} round />
                        </FacebookShareButton>
                      </li>

                      <li className="twitter">
                        <TwitterShareButton
                          url={
                            "https://corper-news-one-more.vercel.app" +
                            "/details?intro=" +
                            props.intro?.replace(/ /g, "-") +
                            "&id=" +
                            id
                          }
                        >
                          <TwitterIcon size={32} round />
                        </TwitterShareButton>
                      </li>

                      <li className="linkedin">
                        <LinkedinShareButton
                          url={
                            "https://corper-news-one-more.vercel.app" +
                            "/details?intro=" +
                            props.intro?.replace(/ /g, "-") +
                            "&id=" +
                            id
                          }
                        >
                          <LinkedinIcon size={32} round />
                        </LinkedinShareButton>
                      </li>

                      <li className="google_plus">
                        <WhatsappShareButton
                          url={
                            "https://corper-news-one-more.vercel.app" +
                            "/details?intro=" +
                            props.intro?.replace(/ /g, "-") +
                            "&id=" +
                            id
                          }
                        >
                          <WhatsappIcon size={32} round />
                        </WhatsappShareButton>
                      </li>
                    </ul>
                  </div>
                  <div className={style.blog_details_content}>
                    <center>
                      <div className={style.featured_img}>
                        <img
                          src={image}
                          alt="blog_single_featured"
                          style={{ width: "100%" }}
                        />
                      </div>

                      <div className={style.entry_content}>
                        <p dangerouslySetInnerHTML={{ __html: content }} />
                        <div
                          dangerouslySetInnerHTML={{ __html: embaded }}
                        ></div>
                        <p>{source}</p>
                      </div>
                    </center>
                  </div>
                  <div className={style.author_box_n_social}>
                    <ul className={style.social_share}>
                      <li className="facebook">
                        <p>Share News</p>
                      </li>
                      <li></li>
                      <li className="facebook">
                        {/* <a
                          href={
                            "https://www.facebook.com/sharer/sharer.php?u=https://corper-news-one-more.vercel.app" +
                            "/details?intro=" +
                            props.intro?.replace(/ /g, "-") +
                            "&id=" +
                            id
                          }
                        >
                          <FaFacebookF />
                        </a> */}

                        <FacebookShareButton
                          url={
                            "https://corper-news-one-more.vercel.app" +
                            "/details?intro=" +
                            props.intro?.replace(/ /g, "-") +
                            "&id=" +
                            id
                          }
                        >
                          <FacebookIcon size={32} round />
                        </FacebookShareButton>
                      </li>

                      <li className="twitter">
                        <TwitterShareButton
                          url={
                            "https://corper-news-one-more.vercel.app" +
                            "/details?intro=" +
                            props.intro?.replace(/ /g, "-") +
                            "&id=" +
                            id
                          }
                        >
                          <TwitterIcon size={32} round />
                        </TwitterShareButton>
                      </li>

                      <li className="linkedin">
                        <LinkedinShareButton
                          url={
                            "https://corper-news-one-more.vercel.app" +
                            "/details?intro=" +
                            props.intro?.replace(/ /g, "-") +
                            "&id=" +
                            id
                          }
                        >
                          <LinkedinIcon size={32} round />
                        </LinkedinShareButton>
                      </li>

                      <li className="google_plus">
                        <WhatsappShareButton
                          url={
                            "https://corper-news-one-more.vercel.app" +
                            "/details?intro=" +
                            props.intro?.replace(/ /g, "-") +
                            "&id=" +
                            id
                          }
                        >
                          <WhatsappIcon size={32} round />
                        </WhatsappShareButton>
                      </li>
                    </ul>
                  </div>
                  <div>
                    <Row>
                      <Col md={6}>
                        <FaAngleLeft className="i" />
                        <a
                          href={
                            "/details/" + url_one?.split("/")[2] + "/" + prevNum
                          }
                          className={style.single_link_previous}
                        >
                          <span className="span">Previous</span>
                          {prev}
                        </a>
                      </Col>
                      <Col md={6} style={{ textAlign: "right" }}>
                        <a
                          href={
                            "/details/" + url_one?.split("/")[2] + "/" + nextNum
                          }
                          className={style.single_link_next}
                        >
                          <span className="span">Next</span>
                          {next}
                        </a>
                        <FaAngleRight className="i" />
                      </Col>
                    </Row>
                  </div>
                  <div className={style.post_pagination}></div>
                  <div className="section section-md bg-white text-black pt-0 line-bottom-light">
                    <div className="container">
                      <div className="row justify-content-center">
                        <div className="col-12 col-lg-12">
                          <h5 className="mb-4 text-left">Comments</h5>
                          {sid === null ? (
                            <div className="row justify-content-center">
                              <div className="col-12 col-sm-6">
                                <p>
                                  <Link href="/login">
                                    <a
                                      // to="/login"
                                      className="btn btn-primary btn-block"
                                    >
                                      <FaUserShield /> Login & Write comment
                                    </a>
                                  </Link>
                                </p>
                              </div>
                            </div>
                          ) : (
                            <div></div>
                          )}
                        </div>
                      </div>
                      <p className="disclaimer">
                        Disclaimer: Comments expressed here do not reflect the
                        opinions of CorperNews24 or any employee thereof.
                      </p>

                      <div className="row">
                        <div className="col-12 d-none">
                          <div className="form-group">
                            <label>Subject</label>

                            <input
                              className="form-control"
                              type="text"
                              name="name"
                              id="name"
                              placeholder="Subject"
                            />
                          </div>
                        </div>

                        <div
                          className="col-12 text-left"
                          style={{ alignItems: "start", textAlign: "left" }}
                        >
                          <div className="form-group">
                            <label>Details Comment</label>{" "}
                            <span className="text-danger">*</span>
                            <textarea
                              className="form-control"
                              name="comment"
                              id="comment"
                              placeholder="Details Comment"
                              required
                              rows="5"
                              onChange={handleComment}
                            ></textarea>
                          </div>
                        </div>
                      </div>

                      <div className="row">
                        <div
                          className="col-6"
                          style={{ alignItems: "start", textAlign: "left" }}
                        >
                          <div className="form-group">
                            <button
                              className="btn btn-success"
                              type="submit"
                              style={{ marginTop: "20px" }}
                              onClick={submitComment}
                            >
                              <i className="fas fa-save"></i> Submit
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <Container className="comment_section">
                    <Row>
                      {postComment.map((data) =>
                        data.parent_id === null ? (
                          <Col md={12} className="comment_box">
                            <div className="d-flex justify-content-between mb-4">
                              <span className="font-small">
                                <a href="#" className="user_info">
                                  <span className="font-weight-bold">
                                    {data.username}
                                  </span>{" "}
                                </a>{" "}
                                &nbsp;&nbsp; {data.time}
                              </span>
                            </div>
                            <p
                              className={style.user_comments}
                              dangerouslySetInnerHTML={{ __html: data.comment }}
                            />
                            <div className="d-flex justify-content-between mb-4">
                              <span>
                                <input
                                  type="text"
                                  className="reply_input"
                                  placeholder="write your reply here..."
                                  onChange={handleRepliedMessgae}
                                />
                                <button
                                  className="reply_button"
                                  onClick={() => replyComment(data.id)}
                                >
                                  Reply
                                </button>
                              </span>
                            </div>
                            {/* <RepliedComment data={data.id} /> */}
                          </Col>
                        ) : (
                          <div></div>
                        )
                      )}
                    </Row>
                  </Container>
                </div>
              </Col>
              <Col md={5} className="single-col post-layout3">
                <Container>
                  <Row>
                    <Col md={12}>
                      <VerticalAds
                        type="News Details -Right 300 x 250"
                        category=""
                      />
                      {/* <GoogleAd
                        slot="8832545708"
                        googleAdId="ca-pub-4422070729845058"
                      /> */}
                      <Trending />
                      <VerticalAds
                        type="News Details -Right 300 x 250"
                        category=""
                      />
                      {/* <GoogleAd
                        slot="8832545708"
                        googleAdId="ca-pub-4422070729845058"
                      /> */}
                    </Col>
                  </Row>
                </Container>
              </Col>
            </Row>
          </Container>
        </div>
      </div>
    </div>
  );
}
export async function getServerSideProps(context) {
  const res = await fetch(
    `${API_URL}` + "?action=GET_NEWS_DETAILS&id=" + context.query.id
  );
  const data = await res.json();

  return {
    props: { props: data }, // will be passed to the page component as props
  };
}
