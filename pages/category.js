import React, { useState, useEffect } from "react";
import Script from "next/script";

import Header from "./common/Header";
import Footer from "./common/Footer";
import { useRouter } from "next/router";
import axios from "axios";
import { API_URL } from "../constants/URL";
import { Container, Col, Row } from "react-bootstrap";
import CategoriesNews from "./common/CategoriesNews";
import Link from "next/link";

import style from "../styles/Category.module.css";
import Trending from "./common/Trending";
import VerticalAds from "../pages/common/VerticalAds";
import HorizontalAds from "../pages/common/HorizontalAds";
export default function Category({ query }) {
  // const id = window.location.pathname.split("?")[2];
  const router = useRouter();
  const [data, setData] = useState([]);
  const [topThird, setTopThird] = useState([]);
  const [id, setId] = useState("");
  useEffect(() => {
    getTopThree();
    setId(router.query.name);
  }, []);
  const getTopThree = () => {
    let formdata = new FormData();
    formdata.append("action", "GET_TRENDING");
    axios({
      method: "post",
      url: `${API_URL}`,
      data: formdata,
    })
      .then(function (response) {
        //

        setTopThird(response.data);
      })
      .catch(function (error) {
        console.log("error=====>", error);
      });
  };

  return (
    <div>
      <Container>
        <div className={style.main_heading_category}>
          <h3 className={style.category_heading}>{router.query.name}</h3>
        </div>
        <Container>
          <HorizontalAds type="CATEGORY Top banner 970 x 90" category={id} />
        </Container>
        <Container fluid className={style.categories_holder}>
          <Row>
            <Col md={8} className={style.news_section}>
              <Row>
                <CategoriesNews id={router.query.name} />
              </Row>
            </Col>
            <Col md={4}>
              <VerticalAds
                type="CATEGORIES First Right 300 x 250"
                category={id}
              />
              <br></br>
              <VerticalAds
                type="CATEGORIES First Second 300 x 250"
                category={id}
              />
              <Trending />
            </Col>
          </Row>
        </Container>
      </Container>
    </div>
  );
}
4;
