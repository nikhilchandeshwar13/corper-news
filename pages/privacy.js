import React, { useState, useEffect } from "react";
import { Container, Col, Row } from "react-bootstrap";
import style from "../styles/Privacy.module.css";
import axios from "axios";
import { API_URL } from "../constants/URL";
import Trending from "./common/Trending";
import Header from "./common/Header";
import Footer from "./common/Footer";
export default function Privacy() {
  const [data, setData] = useState("");
  useEffect(() => {
    getTerms();
  }, []);
  const getTerms = () => {
    let formdata = new FormData();
    formdata.append("action", "GET_PRIVACY");
    axios({
      method: "post",
      url: `${API_URL}`,
      data: formdata,
    })
      .then(function (response) {
        //

        setData(response.data.terms);
      })
      .catch(function (error) {
        console.log("error=====>", error);
      });
  };
  return (
    <div>
      <Container>
        <div className={style.terms_header}>
          <h2 className={style.terms_heading}>Privacy Policy</h2>
          <p
            className={style.terms_detail}
            dangerouslySetInnerHTML={{ __html: data }}
          ></p>
        </div>
      </Container>
    </div>
  );
}
