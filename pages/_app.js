import "bootstrap/dist/css/bootstrap.css";
import "antd/dist/antd.css";
import Head from "next/head";
//import { Navbar, Container, Nav, NavDropdown } from "bootstrap";
import Header from "./common/Header";
import Footer from "./common/Footer";
function MyApp({ Component, pageProps }) {
  return (
    <view>
      <Head>
        <link
          rel="icon"
          type="image/x-icon"
          href="https://corpernews24.com/static/media/mylogoweb.acf3bb67.png"
        />
      </Head>
      <Header />
      <Component {...pageProps} />
      <Footer />
    </view>
  );
}

export default MyApp;
