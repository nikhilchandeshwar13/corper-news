import React, { useState, useEffect } from "react";
import style from "../styles/Aboutus.module.css";
import { Container, Row, Col, Image } from "react-bootstrap";
// import Hori from "../assets/hori.png";
import axios from "axios";
import { API_URL } from "../constants/URL";
import Header from "./common/Header";
import Footer from "./common/Footer";
// import HorizontalAds from "../components/HorizontalAds";
// import GoogleAd from "../components/GoogleAd";
export default function Aboutus() {
  const [about, setAbout] = useState("");
  const [team, setTeam] = useState([]);
  useEffect(() => {
    getAbout();
    getTeam();
  }, []);
  const getAbout = () => {
    let formdata = new FormData();
    formdata.append("action", "GET_ABOUT");
    axios({
      method: "post",
      url: `${API_URL}`,
      data: formdata,
    })
      .then(function (response) {
        //

        setAbout(response.data.about);
      })
      .catch(function (error) {
        console.log("error=====>", error);
      });
  };
  const getTeam = () => {
    let formdata = new FormData();
    formdata.append("action", "GET_TEAMS");
    axios({
      method: "post",
      url: `${API_URL}`,
      data: formdata,
    })
      .then(function (response) {
        //

        setTeam(response.data);
      })
      .catch(function (error) {
        console.log("error=====>", error);
      });
  };
  return (
    <div>
      <Container>
        <div className={style.main_heading_about}>
          <h3 className={style.about_heading}>About Us</h3>
        </div>
        <Container>
          {/* <HorizontalAds type="About us  Top banner 970 x 90" category="" /> */}
        </Container>
        {/* <GoogleAd slot="8832545708" googleAdId="ca-pub-4422070729845058" /> */}
        <Row>
          <Col md={12}>
            <br></br>
            <Row>
              <Col md={2}></Col>
              <Col md={8}>
                <div
                  dangerouslySetInnerHTML={{ __html: about }}
                  style={{ textAlign: "justify" }}
                ></div>
              </Col>
              <Col md={2}></Col>
            </Row>
          </Col>
        </Row>
      </Container>
      <Container>
        <div className={style.main_heading_about}>
          <h3 className={style.about_heading}>Our Team</h3>
          <br></br>
          <Row>
            {team.map((data) => (
              <Col md={4} key={data.name}>
                <Image fluid src={data.image} style={{ height: 400 }} />
                <h3 className={style.team_name}>{data.name}</h3>
                <p className={style.team_position}>{data.position}</p>
                <p className={style.team_email}>{data.email}</p>
              </Col>
            ))}
          </Row>
        </div>
      </Container>
    </div>
  );
}
