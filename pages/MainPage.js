import React from "react";
import Header from "../pages/common/Header";
import Latest from "./common/Latest";
import Top from "./common/Top";
import HomeTrending from "./common/HomeTrending";
import CorperNewsReport from "./common/CorperNewsReport";
import Footer from "./common/Footer";
import HorizontalAds from "../pages/common/HorizontalAds";
import GoogleAd from "../pages/common/GoogleAd";
import Head from "next/head";
import MetaTags from "react-meta-tags";
export default function MainPage() {
  return (
    <div>
      <Head>
        <meta charSet="utf-8" />
        <title>Corper News 24</title>
      </Head>

      <Top />
      <div className="home_ads_section">
        <HorizontalAds type="Home Below TOP 970 x 90" category="" />
      </div>
      <GoogleAd slot="5986890511" googleAdId="ca-pub-4422070729845058" />
      <Latest />
      <div className="home_ads_section">
        <HorizontalAds type="Home Below LATEST 970 x 90" category="" />
      </div>
      <HomeTrending />
      <div className="home_ads_section">
        <HorizontalAds type="Home Below POPULAR 970 x 90" category="" />
      </div>
      <CorperNewsReport />
      <div className="home_ads_section">
        <HorizontalAds
          type="Home Below  CORPER MEDIA REPORTS 970 x 90"
          category=""
        />
      </div>
    </div>
  );
}
