import { Container, Row, Col, Image } from "react-bootstrap";
import Shimmer from "react-shimmer-effect";
import style from "../../styles/HomeLoading.module.css";
export default function HomeLoading() {
  return (
    <Container fluid style={{ marginTop: "50px" }}>
      <Row>
        <Col md={12}>
          <Shimmer>
            <div className={style.first_image}></div>
            <div className={style.line}></div>
            <div className={style.line_one}></div>
            <div className={style.line}></div>
          </Shimmer>
          <Shimmer>
            <div className={style.first_image}></div>
            <div className={style.line}></div>
            <div className={style.line_one}></div>
            <div className={style.line}></div>
          </Shimmer>
          <Shimmer>
            <div className={style.first_image}></div>
            <div className={style.line}></div>
            <div className={style.line_one}></div>
            <div className={style.line}></div>
          </Shimmer>
          <Shimmer>
            <div className={style.first_image}></div>
            <div className={style.line}></div>
            <div className={style.line_one}></div>
            <div className={style.line}></div>
          </Shimmer>
          <Shimmer>
            <div className={style.first_image}></div>
            <div className={style.line}></div>
            <div className={style.line_one}></div>
            <div className={style.line}></div>
          </Shimmer>
          <Shimmer>
            <div className={style.first_image}></div>
            <div className={style.line}></div>
            <div className={style.line_one}></div>
            <div className={style.line}></div>
          </Shimmer>
        </Col>
      </Row>
    </Container>
  );
}
