import React, { useState, useEffect } from "react";
//import Hori from "../assets/hori.png";
import axios from "axios";
import { API_URL } from "../../constants/URL";
import { MobileView, BrowserView } from "react-device-detect";
export default function VerticalAds({ type, category }) {
  const [image, setImage] = useState("");
  const [url, setUrl] = useState("");
  const [mobile, setMobile] = useState("");
  useEffect(() => {
    getAds();
  }, []);
  const getAds = () => {
    let formdata = new FormData();
    formdata.append("action", "GET_VERTICAL_ADS");
    formdata.append("type", type);
    formdata.append("category", category);
    axios({
      method: "post",
      url: `${API_URL}`,
      data: formdata,
    })
      .then(function (response) {
        if (response.data.status === "success") {
          setImage(response.data.image);
          setUrl(response.data.url);
          setMobile(response.data.mobile);
        } else {
        }
      })
      .catch(function (error) {
        console.log("error=====>", error);
      });
  };
  if (image === "") {
    return <div></div>;
  } else {
    return (
      <div style={{ width: "100%" }}>
        <BrowserView>
          <a href={url}>
            <img src={image} style={{ width: "100%", height: "250px" }} />
          </a>
        </BrowserView>
        <MobileView>
          <a href={url}>
            <img src={mobile} style={{ width: "100%", height: "250px" }} />
          </a>
        </MobileView>
      </div>
    );
  }
}
