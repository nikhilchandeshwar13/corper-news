import React from "react";
import { Pagination } from "react-bootstrap";
import { Col, Row } from "react-bootstrap";
export default function PaginationFun({ postPerPage, totalPost, paginate }) {
  const pageNumber = [];

  for (let i = 1; i <= Math.ceil(totalPost / postPerPage); i++) {
    pageNumber.push(i);
  }
  return (
    <Row>
      <Col md={8}>
        <nav style={{ marginTop: 20 }}>
          <ul className="pagination float-right">
            {pageNumber.map((number) => (
              <li key={number} className="page-item custom-paginate">
                <a
                  onClick={() => paginate(number)}
                  href="#"
                  className="page-link"
                  style={{ color: "#f00606" }}
                >
                  {number}
                </a>
              </li>
            ))}
          </ul>
        </nav>
      </Col>
    </Row>
  );
}
