import React, { useEffect } from "react";

export default function GoogleAd() {
  const loadAds = () => {
    try {
      if (typeof window !== "undefined") {
        (window.adsbygoogle = window.adsbygoogle || []).push({});
      }
    } catch (error) {
      console.log("adsense error", error.message);
    }
  };

  useEffect(() => {
    loadAds();
  }, []);

  return (
    <ins
      className="adsbygoogle"
      style={{ display: "block" }}
      data-ad-client="ca-pub-4422070729845058"
      data-ad-slot="5986890511"
      data-ad-format="auto"
      data-full-width-responsive="true"
    ></ins>
  );
}
