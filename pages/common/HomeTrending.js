import React, { useState, useEffect } from "react";
import { Container, Row, Col, Image } from "react-bootstrap";
import { API_URL } from "../../constants/URL";
import style from "../../styles/HomeTrending.module.css";
import axios from "axios";
import HomeLoading from "./HomeLoading";
export default function HomeTrending() {
  const [topTwo, setTopTwo] = useState([]);
  const [topSecond, setTopSecond] = useState([]);
  const [topThird, setTopThird] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  useEffect(() => {
    getToptwo();
    getTopSecond();
    getTopThree();
  }, []);
  const getToptwo = () => {
    setIsLoading(true);
    let formdata = new FormData();
    formdata.append("action", "GET_TOP_TRENDING");
    axios({
      method: "post",
      url: `${API_URL}`,
      data: formdata,
    })
      .then(function (response) {
        //

        setTopTwo(response.data);
        setIsLoading(false);
      })
      .catch(function (error) {
        console.log("error=====>", error);
      });
  };
  const getTopSecond = () => {
    setIsLoading(true);
    let formdata = new FormData();
    formdata.append("action", "GET_TRENDING_TWO");
    axios({
      method: "post",
      url: `${API_URL}`,
      data: formdata,
    })
      .then(function (response) {
        //

        setTopSecond(response.data);
        setIsLoading(false);
      })
      .catch(function (error) {
        console.log("error=====>", error);
      });
  };
  const getTopThree = () => {
    setIsLoading(true);
    let formdata = new FormData();
    formdata.append("action", "GET_TOP_TRENDING_THREESECTION");
    axios({
      method: "post",
      url: `${API_URL}`,
      data: formdata,
    })
      .then(function (response) {
        //

        setTopThird(response.data);
        setIsLoading(false);
      })
      .catch(function (error) {
        console.log("error=====>", error);
      });
  };
  if (isLoading) {
    return (
      <div>
        <h2 className={style.trending_heading}>POPULAR</h2>
        <HomeLoading />
      </div>
    );
  } else {
    return (
      <div>
        <Container fluid>
          <div className={style.section_title}>
            <h2 className={style.trending_heading}>POPULAR</h2>
            <br></br> <br></br> <br></br>
            <Container fluid>
              <div className={style.post_layout_wrapper}>
                <Row>
                  <Col md={5} className={style.post_layout1}>
                    <Row>
                      {topTwo.map((data) => (
                        <Col md={12} key={data.name}>
                          <div className={style.single_post}>
                            <div className={style.img_area}>
                              <a
                                href={
                                  "/details?intro=" +
                                  data.intro?.replace(/ /g, "-") +
                                  "&id=" +
                                  data.id
                                }
                              >
                                <Image
                                  src={data.image}
                                  className={style.img}
                                  fluid
                                />
                              </a>
                            </div>
                            <h1 className={style.post_title}>
                              <a
                                className={style.a_anchor}
                                href={
                                  "/details?intro=" +
                                  data.intro?.replace(/ /g, "-") +
                                  "&id=" +
                                  data.id
                                }
                              >
                                {data.name}
                              </a>
                            </h1>
                            <span className={style.tag}>{data.category}</span>
                            <p className={style.description}>{data.intro}</p>
                          </div>
                        </Col>
                      ))}
                    </Row>
                  </Col>
                  <Col md={3} className={style.post_layout2}>
                    <Row>
                      {topSecond.map((data) => (
                        <Col md={12} key={data.name}>
                          <div className={style.post_two}>
                            <div className={style.img_area}>
                              <a
                                href={
                                  "/details?intro=" +
                                  data.intro?.replace(/ /g, "-") +
                                  "&id=" +
                                  data.id
                                }
                              >
                                <Image
                                  src={data.image}
                                  alt="feature image"
                                  className={style.img}
                                  fluid
                                />
                              </a>
                            </div>
                            <h1 className={style.post_title}>
                              <a
                                className={style.a_anchor}
                                href={
                                  "/details?intro=" +
                                  data.intro?.replace(/ /g, "-") +
                                  "&id=" +
                                  data.id
                                }
                              >
                                {data.name}
                              </a>
                            </h1>
                            <span className={style.tag}>{data.category}</span>
                            <p className={style.description}>{data.intro}</p>
                          </div>
                        </Col>
                      ))}
                    </Row>
                  </Col>
                  <Col md={4} className={style.post_layout3}>
                    <Row>
                      <Col md={12}>
                        {/* <VerticalAds
                      type="Home LATEST top right 300 x 250"
                      category=""
                    /> */}
                        {/* <GoogleAd
                    slot="9269074687"
                    googleAdId="ca-pub-4422070729845058"
                  /> */}
                        <Row>
                          {topThird.map((data) => (
                            <Col md={12} key={data.name}>
                              <div className={style.single_post}>
                                <div className={style.img_area}>
                                  <a
                                    className={style.a_anchor}
                                    href={
                                      "/details?intro=" +
                                      data.intro?.replace(/ /g, "-") +
                                      "&id=" +
                                      data.id
                                    }
                                  >
                                    <img
                                      src={data.image}
                                      alt="feature image"
                                      className={style.img}
                                    />
                                  </a>
                                </div>

                                <div className={style.post_content}>
                                  <span className={style.type}>HOT</span>
                                  <h4 className={style.post_title}>
                                    <a
                                      className={style.third_anchor}
                                      href={
                                        "/details?intro=" +
                                        data.intro?.replace(/ /g, "-") +
                                        "&id=" +
                                        data.id
                                      }
                                    >
                                      {data.name}
                                    </a>
                                  </h4>
                                  <span className={style.tag}>
                                    {data.category}
                                  </span>
                                  <p>{data.published_at}</p>
                                </div>
                              </div>
                            </Col>
                          ))}
                        </Row>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </div>
            </Container>
          </div>
        </Container>
      </div>
    );
  }
}
