import React, { useState, useEffect } from "react";

import Script from "next/script";

import { withRouter } from "next/router";
import Header from "../common/Header";
import { useRouter } from "next/router";
import axios from "axios";
import { API_URL } from "../../constants/URL";
import { Container, Col, Row } from "react-bootstrap";
import Head from "next/head";
import Link from "next/link";
import style from "../../styles/Trending.module.css";

export default function Trending({ query }) {
  // const id = window.location.pathname.split("?")[2];
  const router = useRouter();
  const [data, setData] = useState([]);
  const [topThird, setTopThird] = useState([]);
  const [id, setId] = useState(null);
  useEffect(() => {
    //lert(router.query.name);
    getTopThree();
  }, []);
  const getTopThree = () => {
    let formdata = new FormData();
    formdata.append("action", "GET_TRENDING");
    axios({
      method: "post",
      url: `${API_URL}`,
      data: formdata,
    })
      .then(function (response) {
        //

        setTopThird(response.data);
      })
      .catch(function (error) {
        console.log("error=====>", error);
      });
  };

  return (
    <div>
      <Head>
        <meta charset="utf-8" />
        <meta charset="utf8" />
      </Head>
      <Row>
        <Col md={12}>
          {/* <VerticalAds
              type="CATEGORIES First Right 300 x 250"
              category={id}
            /> */}
          <br></br>
          {/* <VerticalAds
              type="CATEGORIES First Second 300 x 250"
              category={id}
            />
            
            <GoogleAd slot="8832545708" googleAdId="ca-pub-4422070729845058" />
            <GoogleAd slot="8832545708" googleAdId="ca-pub-4422070729845058" /> */}
          <Script
            id="8832545708"
            data-ad-client="ca-pub-4422070729845058"
            async
            strategy="rectangle"
            onError={(e) => {
              console.error("Script failed to load", e);
            }}
            src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"
          />
          <div>
            <h3 className={style.trending_text}>Trending</h3>
          </div>
          <Row>
            {topThird.map((data) => (
              <Col md={12} key={data.name}>
                <div className={style.single_post}>
                  <div className={style.img_area}>
                    <a
                      className={style.a_anchor}
                      // href=""
                      // // to={{
                      // //   pathname:
                      // //     "/details/" +
                      // //     data.intro.replace(/ /g, "-") +
                      // //     "/" +
                      // //     data.id,
                      // // }}
                      href={
                        "/details?intro=" +
                        data.intro?.replace(/ /g, "-") +
                        "&id=" +
                        data.id
                      }
                    >
                      <img
                        src={data.image}
                        alt="feature image"
                        className={style.img}
                      />
                    </a>
                  </div>

                  <div className={style.post_content}>
                    <h4 className={style.post_title}>
                      <a
                        href={
                          "/details?intro=" +
                          data.intro?.replace(/ /g, "-") +
                          "&id=" +
                          data.id
                        }
                        className={style.third_anchor}
                      >
                        {data.name
                          ?.replace(/&/g, "&amp;")
                          .replace(/</g, "&lt;")
                          .replace(/>/g, "&gt;")
                          .replace(/"/g, "&quot;")
                          .replace(/'/g, "&#039;")}
                        {/* <div
                          dangerouslySetInnerHTML={{ __html: data.name }}
                        ></div> */}
                      </a>
                    </h4>
                    <span className={style.tag}>{data.category}</span>
                    <p>{data.published_at}</p>
                  </div>
                </div>
              </Col>
            ))}
          </Row>
          {/* <GoogleAd slot="6314015967" googleAdId="ca-pub-4422070729845058" />
            <GoogleAd slot="6314015967" googleAdId="ca-pub-4422070729845058" />
            <GoogleAd slot="6314015967" googleAdId="ca-pub-4422070729845058" />
            <GoogleAd slot="6314015967" googleAdId="ca-pub-4422070729845058" /> */}
        </Col>
      </Row>
    </div>
  );
}
