import React, { useState, useEffect } from "react";

import axios from "axios";
import { API_URL } from "../../constants/URL";
import { Image } from "react-bootstrap";
import { Picture } from "react-responsive-picture";
import { MobileView, BrowserView } from "react-device-detect";
export default function HorizontalAds({ type, category }) {
  const [image, setImage] = useState("");
  const [url, setUrl] = useState("");
  const [mobile, setMobile] = useState("");
  useEffect(() => {
    getAds();
  }, []);
  const getAds = () => {
    let formdata = new FormData();
    formdata.append("action", "GET_HORIZONTAL_ADS");
    formdata.append("type", type);
    formdata.append("category", category);
    axios({
      method: "post",
      url: `${API_URL}`,
      data: formdata,
    })
      .then(function (response) {
        if (response.data.status === "success") {
          setImage(response.data.image);
          setUrl(response.data.url);
          setMobile(response.data.mobile);
        } else {
        }
      })
      .catch(function (error) {
        console.log("error=====>", error);
      });
  };
  if (image === "") {
    return <div></div>;
  } else {
    return (
      <div>
        <BrowserView>
          <a href={url}>
            {/* <Image src={image} fluid style={{ width: 728 }} /> */}
            <Picture
              src={image}
              sizes="(min-width: 36em) 33.3vw, 100vw"
              style={{ width: 728 }}
            />
          </a>
        </BrowserView>
        <MobileView>
          <a href={url}>
            {/* <Image src={image} fluid style={{ width: 728 }} /> */}
            <Image
              fluid
              src={mobile}
              // sizes="(min-width: 36em) 33.3vw, 100vw"
              // style={{ width: 728 }}
            />
          </a>
        </MobileView>
      </div>
    );
  }
}
