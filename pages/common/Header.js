import React, { useState, useEffect } from "react";
import Image from "next/image";
import logo from "../../public/logo.png";
import axios from "axios";
import { API_URL } from "../../constants/URL";
import { Navbar, Container, Nav, NavDropdown } from "react-bootstrap";
import style from "../../styles/Navbar.module.css";
import Link from "next/link";
//import { Navbar, Container, Nav, NavDropdown } from "bootstrap";
export default function Header() {
  const [data, setData] = useState([]);
  const [sid, setSid] = useState(null);
  useEffect(() => {
    getCategories();
    setSid(localStorage.getItem("username"));
  }, []);
  const getCategories = () => {
    let formdata = new FormData();
    formdata.append("action", "GET_NAV_CATEGORIES");
    axios({
      method: "post",
      url: `${API_URL}`,
      data: formdata,
    })
      .then(function (response) {
        console.log(response.data);

        setData(response.data);
      })
      .catch(function (error) {
        console.log("error=====>", error);
      });
  };
  return (
    <Navbar bg="white" expand="lg" className={style.navbar}>
      <Container>
        <Navbar.Brand href="/">
          <img
            src="https://corpernews24.com/static/media/mylogoweb.acf3bb67.png"
            className={style.logo}
            style={{ height: "90px" }}
          />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ms-auto">
            <Nav.Link href="/" className={style.a_one}>
              Home
            </Nav.Link>
            {data.map((data) =>
              data.name === "LATEST" ? (
                <span></span>
              ) : (
                <Nav.Link href={"/category?name=" + data.name}>
                  <a
                    href={"/category?name=" + data.name}
                    className={style.a_one}
                  >
                    {data.name}
                  </a>
                </Nav.Link>
              )
            )}
            <Nav.Link href="/videos" className={style.a_one}>
              Videos
            </Nav.Link>
            {sid === null ? (
              <>
                <Nav.Link href="/register" className={style.header_button}>
                  Join
                </Nav.Link>
                <Nav.Link href="/login" className={style.header_button}>
                  Login
                </Nav.Link>
              </>
            ) : (
              <>
                <Nav.Link
                  onClick={() => logout()}
                  className={style.header_button}
                >
                  Logout
                </Nav.Link>
              </>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
