import React from "react";
import { Col, Row, Image, Container } from "react-bootstrap";
import styles from "../../styles/CategoriesNews.module.css";
export default function CategoryPost({ posts, cid }) {
  return (
    <div>
      {posts?.map((data) => (
        <Container className={styles.category_news_holder} key={data.id}>
          <Row>
            <Col md={6}>
              <a
                href={
                  "/details?intro=" +
                  data.intro?.replace(/ /g, "-") +
                  "&id=" +
                  data.id
                }
              >
                <Image
                  src={data.image}
                  fluid
                  className={styles.category_news_image}
                />
              </a>
            </Col>
            <Col md={6} className={styles.postContent}>
              <a
                href={
                  "/details?intro=" +
                  data.intro?.replace(/ /g, "-") +
                  "&id=" +
                  data.id
                }
                className={styles.post_a}
              >
                <h3
                  style={{
                    fontWeight: 700,
                    fontSize: 20,
                    lineHeight: 1.4,
                  }}
                >
                  {data.name}
                </h3>
              </a>
              <p className={styles.tag}>{cid}</p>
              <p>
                <span>{data.created_by}</span>
                {"   "}
                <span>Published {data.published_at}</span>
              </p>
              <ul className={styles.meta}>
                <li className={styles.meta_li}></li>
                <li className={styles.meta_li}></li>
              </ul>
              <p dangerouslySetInnerHTML={{ __html: data.intro }} />
            </Col>
          </Row>
        </Container>
      ))}
    </div>
  );
}
