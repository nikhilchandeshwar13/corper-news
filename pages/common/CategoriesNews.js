import React, { useEffect, useState, useCallback } from "react";

import { Col, Row, Image, Container } from "react-bootstrap";
import axios from "axios";
import { API_URL } from "../../constants/URL";
// import PaginationFun from "./PaginationFun";
import { Pagination } from "antd";
import Link from "next/link";
import CategoryPost from "./CategoryPost";
import CategoryNewsLoading from "./CategoryNewsLoading";
// import { Link } from "react-router-dom";
//import { decode } from "html-entities";
// import CategoryPost from "./CategoryPost";
// import PaginationFun from "./PaginationFun";
// import VerticalAds from "./VerticalAds";
import styles from "../../styles/CategoriesNews.module.css";
import { useRouter } from "next/router";
export default function CategoriesNews(id) {
  const [mydata, setData] = useState([]);
  const router = useRouter();
  const [currentPage, setCurrentPage] = useState(1);
  const [postPerPage, setPostPerPage] = useState(10);

  const [cateId, setCateId] = useState(null);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    //setCateId();
    //alert(router.asPath?.split("=")[1]);
    getCategoriesNews(router.asPath?.split("=")[1]);
  }, []);

  const getCategoriesNews = (id) => {
    setIsLoading(true);
    let formdata = new FormData();
    formdata.append("action", "GET_CATEGORIES_NEWS");
    formdata.append("id", id);
    axios({
      method: "post",
      url: `${API_URL}`,
      data: formdata,
    })
      .then(function (response) {
        //
        console.log("response========>", response.data);
        // if (response.data === "[[]]") {
        //   getCategoriesNews(cateId);
        // } else {
        //   setData(response.data);
        //   setIsLoading(false);
        // }
        if (typeof response.data === "string") {
          //alert("Yes");
          //getCategoriesNews(cateId);
        } else {
          setData(response.data);
          setIsLoading(false);
        }
        setIsLoading(false);
      })
      .catch(function (error) {
        getCategoriesNews(id);
        console.log("error=====>", error);
      });
  };

  const indexOfLastPost = currentPage * postPerPage;
  const indexOfFirstPost = indexOfLastPost - postPerPage;
  const currentPost = mydata.slice(indexOfFirstPost, indexOfLastPost);
  const paginate = (pageNumber) => setCurrentPage(pageNumber);
  const updateData = (pageNumber) => {
    setCurrentPage(pageNumber);
    window.scrollTo(0, 0);
  };
  if (isLoading) {
    return <CategoryNewsLoading />;
  } else {
    return (
      <Container>
        <Row>
          <CategoryPost
            posts={currentPost}
            cid={router.asPath?.split("=")[1]}
          />
          <Col md={8}>
            {/* <PaginationFun
              postPerPage={postPerPage}
              totalPost={mydata.length}
              paginate={paginate}
            /> */}
          </Col>
          <Pagination
            onChange={(value) => updateData(value)}
            // pageSize={postPerPage}
            total={mydata.length}
            current={currentPage}
          />
        </Row>
      </Container>
    );
  }
}
