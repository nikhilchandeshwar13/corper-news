import React, { useState, useEffect } from "react";
import { Navbar, Container, Nav, NavDropdown, Row, Col } from "react-bootstrap";
import logo from "../../public/logo.png";
import style from "../../styles/Footer.module.css";
import axios from "axios";
import { API_URL } from "../../constants/URL";
import android from "../../public/google-play-badge.png";
import apple from "../../public/apple.png";
import { useCookies } from "react-cookie";
import { MobileView } from "react-device-detect";
import Image from "next/image";
import Link from "next/link";
// import GoogleAd from "./GoogleAd";
import {
  FaFacebookF,
  FaTwitter,
  FaInstagram,
  FaLinkedin,
  FaYoutube,
} from "react-icons/fa";

export default function Footer() {
  const [isAllowed, setIsAllowed] = useState(null);
  const [cookies, setCookie] = useCookies(["user"]);
  useEffect(() => {
    // alert(cookies.isAllowed);
    var date = new Date().toISOString();
    // date.setTime(date.getTime() + 90 * 24 *

    // alert(date);
  }, []);
  const updateStatus = () => {
    setIsAllowed("yes");
    localStorage.setItem("isAllowed", "yes");
    var date = new Date();
    date.setDate(date.getDate() + 90);

    setCookie("isAllowed", "yes", { maxAge: date });
    window.location.href = "/";
  };
  return (
    <div>
      <Container fluid className={style.footer}>
        <Row>
          <Col md={12}>
            <ul className={style.footer_menu}>
              <li className={style.footer_menu_li}>
                <Link href="https://play.google.com/store/apps/details?id=com.corpernews.android&hl=en_IN&gl=US">
                  <a className={style.footer_menu_a}>
                    <img
                      src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/78/Google_Play_Store_badge_EN.svg/2560px-Google_Play_Store_badge_EN.svg.png"
                      style={{ width: "220px", height: "70px" }}
                    />
                  </a>
                </Link>
              </li>
              <li className={style.footer_menu_li}>
                <Link href="https://apps.apple.com/in/app/corper-news/id1589899278">
                  <a className={style.footer_menu_a}>
                    <img
                      src="https://www.nicepng.com/png/detail/17-179971_itunes-app-store-logo.png"
                      style={{ width: "220px", height: "80px" }}
                    />
                  </a>
                </Link>
              </li>
            </ul>
          </Col>
          <Col md={12}>
            <ul className={style.footer_menu}>
              <li href="/aboutus" className={style.footer_menu_li}>
                <a className={style.footer_menu_a}>About Us</a>
              </li>
              <li className={style.footer_menu_li}>
                <Link href="/contact_us">
                  <a className={style.footer_menu_a}>Contact us</a>
                </Link>
              </li>
              <li className={style.footer_menu_li}>
                <Link href="/advertise">
                  <a className={style.footer_menu_a}>ADVERTISE WITH US</a>
                </Link>
              </li>
              <li className={style.footer_menu_li}>
                <Link href="/career">
                  <a className={style.footer_menu_a}>CAREER</a>
                </Link>
              </li>
              <li className={style.footer_menu_li}>
                <Link href="/privacy">
                  <a className={style.footer_menu_a}>Privacy Policy</a>
                </Link>
              </li>
              <li className={style.footer_menu_li}>
                <Link href="/terms">
                  <a className={style.footer_menu_a}>Terms and Condition</a>
                </Link>
              </li>
              {/* <li className={style.footer_menu_li}>
                <a href="/sitemap" className={style.footer_menu_a}>
                  Sitemap
                </a>
              </li> */}
            </ul>
          </Col>
          <Col md={12}>
            <ul className={style.footer_menu}>
              <li className={style.footer_menu_li}>
                <Link href="https://www.facebook.com/corpernews24">
                  <a className={style.footer_menu_a}>
                    <FaFacebookF />
                  </a>
                </Link>
              </li>
              <li className={style.footer_menu_li}>
                <Link href="https://twitter.com/corpernews">
                  <a className={style.footer_menu_a}>
                    <FaTwitter />
                  </a>
                </Link>
              </li>
              <li className={style.footer_menu_li}>
                <Link href="https://www.instagram.com/corpernews24/">
                  <a className={style.footer_menu_a}>
                    <FaInstagram />
                  </a>
                </Link>
              </li>
              <li className={style.footer_menu_li}>
                <Link href="https://www.linkedin.com/in/corpernews24">
                  <a className={style.footer_menu_a}>
                    <FaLinkedin />
                  </a>
                </Link>
              </li>
              <li className={style.footer_menu_li}>
                <Link href="https://www.youtube.com/channel/UCsgphM-o-WbgJgFHePpsALQ">
                  <a className={style.footer_menu_a}>
                    <FaYoutube />
                  </a>
                </Link>
              </li>
            </ul>
          </Col>
          <p style={{ textAlign: "center" }}>
            Copyright &copy; 2022 Corpernews24 all rights reserved.
          </p>
        </Row>
      </Container>
      <br></br>
      <br></br>
      <br></br>
      {cookies.isAllowed === undefined ? (
        <div className="bottom_cookies center">
          <Navbar
            bg="dark"
            expand="lg"
            fixed="bottom"
            className="bg-light"
            style={{
              minHeight: 70,
              backgroundColor: "#02141f",
              width: "100%",
            }}
          >
            <MobileView>
              {/* <GoogleAd
                slot="5986890511"
                googleAdId="ca-pub-4422070729845058"
              /> */}
            </MobileView>
            <Container
              className="center"
              fluid
              style={{
                textAlign: "center",
              }}
            >
              <div
                style={{
                  width: "100%",
                  color: "#fff",
                  textAlign: "center",
                }}
              >
                Your experience on this site will be improved by allowing
                cookies.{" "}
                <button
                  type="button"
                  className="btn btn-primary btn-sm js-cookie-consent-agree cookie-consent__agree"
                  onClick={() => updateStatus()}
                >
                  Allow cookies
                </button>
              </div>
            </Container>
          </Navbar>
        </div>
      ) : (
        <div></div>
      )}
    </div>
  );
}
