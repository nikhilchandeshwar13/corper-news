import React, { useState, useEffect } from "react";
import style from "../styles/Contact.module.css";
import { Container, Row, Col, Image, Form } from "react-bootstrap";
//import Hori from "../assets/hori.png";

import axios from "axios";
import { API_URL } from "../constants/URL";
// import VerticalAds from "../components/VerticalAds";
// import { Link, Redirect } from "react-router-dom";
// import GoogleAd from "../components/GoogleAd";
import {
  FaEnvelope,
  FaTwitter,
  FaInstagram,
  FaLinkedin,
  FaYoutube,
  FaFacebookF,
} from "react-icons/fa";
// import HorizontalAds from "../components/HorizontalAds";
import Link from "next/link";
import Trending from "./common/Trending";
import Header from "./common/Header";
import Footer from "./common/Footer";
export default function Career() {
  const [topThird, setTopThird] = useState([]);
  const [carrer, setCarrer] = useState("");
  useEffect(() => {
    getTopThree();
    getCarrer();
  }, []);
  const getCarrer = () => {
    let formdata = new FormData();
    formdata.append("action", "GET_CARRER");
    axios({
      method: "post",
      url: `${API_URL}`,
      data: formdata,
    })
      .then(function (response) {
        //

        setCarrer(response.data.carrer);
      })
      .catch(function (error) {
        console.log("error=====>", error);
      });
  };
  const getTopThree = () => {
    let formdata = new FormData();
    formdata.append("action", "GET_TRENDING");
    axios({
      method: "post",
      url: `${API_URL}`,
      data: formdata,
    })
      .then(function (response) {
        //

        setTopThird(response.data);
      })
      .catch(function (error) {
        console.log("error=====>", error);
      });
  };
  return (
    <div>
      <Container>
        <div className={style.main_heading_about}>
          <h3 className={style.about_heading}>Career</h3>
        </div>
        <Container>
          {/* <HorizontalAds type="Career Top banner 970 x 90" category="" /> */}
        </Container>
        {/* <GoogleAd slot="5986890511" googleAdId="ca-pub-4422070729845058" /> */}
        <Row>
          <Col md={8}>
            <Container>
              <Row>
                <Col md={8}>
                  <div
                    dangerouslySetInnerHTML={{ __html: carrer }}
                    style={{ textAlign: "justify" }}
                  ></div>
                </Col>
              </Row>
            </Container>
          </Col>
          <Col md={4} className={style.post_layout3}>
            <Row>
              <Col md={12}>
                {/* <VerticalAds /> */}
                <div>
                  {/* <h3 className={style.trending_text}>Trending</h3> */}
                </div>
                <Row>
                  <Trending />
                </Row>
              </Col>
            </Row>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
