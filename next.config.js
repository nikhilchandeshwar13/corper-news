/** @type {import('next').NextConfig} */

const nextConfig = {
  reactStrictMode: true,
  images: {
    loader: "akamai",
    path: "/",
  },

  exportTrailingSlash: true,
  trailingSlash: true,
  //pageExtensions: ["page.tsx", "page.ts", "page.jsx", "page.js"],
};

module.exports = nextConfig;
// module.exports = withImages({
//   esModule: true,
// });
// module.exports = {
//   pageExtensions: ["page.tsx", "page.ts", "page.jsx", "page.js"],
// };
